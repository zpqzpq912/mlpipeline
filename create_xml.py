from lxml import etree
class GEN_Annotations:
    def __init__(self, filename):
        self.root = etree.Element("annotation")
        child1 = etree.SubElement(self.root, "folder")
        child1.text = "VOC2012"
        child2 = etree.SubElement(self.root, "filename")
        child2.text = filename
        child2 = etree.SubElement(self.root, "path")
        child2.text = "XML path must be stored in English."
        child3 = etree.SubElement(self.root, "source")
        child5 = etree.SubElement(child3, "database")
        child5.text = "Unknown"
        #tree1 = etree.parse(filename)
        #self.root = tree1.getroot()
    def set_size(self,witdh,height,channel):
        size = etree.SubElement(self.root, "size")
        widthn = etree.SubElement(size, "width")
        widthn.text = str(witdh)
        heightn = etree.SubElement(size, "height")
        heightn.text = str(height)
        channeln = etree.SubElement(size, "depth")
        channeln.text = str(channel)

    def savefile(self,filename):
        tree = etree.ElementTree(self.root)
        tree.write(filename, pretty_print=True, xml_declaration=False, encoding='utf-8')

    def add_pic_attr(self,label,xmin,ymin,xmax,ymax, score):
        object = etree.SubElement(self.root, "object")
        namen = etree.SubElement(object, "name")
        namen.text = label
        pose = etree.SubElement(object, "pose")
        pose.text = str(0)
        truncated = etree.SubElement(object, "truncated")
        truncated.text = str(0)
        difficult = etree.SubElement(object, "difficult")
        difficult.text = str(0)
        bndbox = etree.SubElement(object, "bndbox")
        xminn = etree.SubElement(bndbox, "xmin")
        xminn.text = str(xmin)
        yminn = etree.SubElement(bndbox, "ymin")
        yminn.text = str(ymin)
        xmaxn = etree.SubElement(bndbox, "xmax")
        xmaxn.text = str(xmax)
        ymaxn = etree.SubElement(bndbox, "ymax")
        ymaxn.text = str(ymax)
        scoren = etree.SubElement(object, "score")
        scoren.text = str(score)

    def genvoc(filename,class_,width,height,depth,xmin,ymin,xmax,ymax,savedir):
        anno = GEN_Annotations(filename)
        anno.set_size(width,height,depth)
        anno.add_pic_attr("pos", xmin, ymin, xmax, ymax)
        anno.savefile(savedir)

if __name__ == '__main__':
    filename="000001.jpg"
    anno= GEN_Annotations(filename)
    anno.set_size(1280,720,3)
    for i in range(3):
        xmin=i+1
        ymin=i+10
        xmax=i+100
        ymax=i+100
        anno.add_pic_attr("pos",xmin,ymin,xmax,ymax)
        anno.savefile("00001.xml")