import os
import shutil
import argparse

'''
Name the data uniformly
'''

def get_all_xml_jpg(rt_path):
    #Picture output directory
    out_jpg_path = os.path.join(rt_path, 'JPEGImages')
    if not os.path.exists(out_jpg_path):
        os.makedirs(out_jpg_path)
    #xml output directory
    out_xml_path = os.path.join(rt_path, 'Annotations')
    if not os.path.exists(out_xml_path):
        os.makedirs(out_xml_path)
    num = 0
    for rt,dirs,files in os.walk(rt_path):
        for file in files:
            if file.split('.')[1] == 'xml':
                #named as "capstone + data + num"
                new_jpg_name = "capstone_1025_" + str(num) + ".jpg"
                new_xml_name = "capstone_1025_" + str(num) + ".xml"
                if os.path.exists(os.path.join(rt, file.replace('.xml', '.jpg'))):
                    shutil.move(os.path.join(rt, file), os.path.join(out_xml_path, new_xml_name))
                    shutil.move(os.path.join(rt, file.replace('.xml', '.jpg')), os.path.join(out_jpg_path, new_jpg_name))
                    num += 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--root_path', type=str, default='', help='all xml and image file path')
    opt = parser.parse_args()
    get_all_xml_jpg(opt.root_path)