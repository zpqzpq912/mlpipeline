# -*-coding:utf-8-*-
# check xml format

from lxml import etree
import os
import glob
import re
from PIL import Image
import shutil
import argparse

zh_pattern = re.compile(u'[\u4e00-\u9fa5]')


class CheckXml():

    def __init__(self, xml_file, img_file):
        self.xml = xml_file
        self.img = img_file
        self.tree = etree.parse(xml_file)
        self.root = self.tree.getroot()

    def check_folder(self):

        if self.root[0].text != 'VOC2012':
            self.root[0].text = 'VOC2012'
            print('%s changed <folder>' % os.path.basename(self.xml))
            self.tree.write(self.xml)
    def check_size(self):
        #print(self.img)
        try:
            im = Image.open(self.img)
            width,height = im.size
            for node in self.root:
                if node.tag=='size':
                    if node[0].text != str(width):
                        print(self.img)
                        node[0].text=str(width)
                        node[1].text=str(height)

            self.tree.write(self.xml)
        except:
            out_xml = self.xml.replace("/xml","/out")
            print(out_xml)
            shutil.move(self.xml,out_xml)

    def check_filename(self):

        img_name = os.path.basename(self.img)
        xml_filename = self.root[1].text
        if xml_filename == img_name:
            pass
        else:
            self.root[1].text = img_name
            self.tree.write(self.xml)

    def contain_zh(self, word):

        #print(word)
        global zh_pattern
        match = zh_pattern.search(word)
        return match

    def check_chn(self):

        #print(self.xml)
        if self.contain_zh(self.root[2].text):
            self.root[2].text = 'XML path must be stored in English.'
            self.tree.write(self.xml)

    def check_object(self, label_lst, if_delete=False):

        if self.root.findall('object'):

            for obj_name in self.tree.xpath('//name'):
                if obj_name.text in label_lst:
                    continue
                else:
                    print('Wrong label:', obj_name.text)
                    with open('wrong_label.txt', 'a') as f:
                        f.write(obj_name.text + '\t' + os.path.basename(self.xml) + '\n')

        else:
            if if_delete:
                print("%s delete!" % self.xml)
                os.remove(self.xml)
                os.remove(self.img)
            else:
                print('%s Objects not found.' % self.xml)


    def pick_object(self, select_lst):

        for obj in self.root.findall('object'):
            #if obj[0].text == 'void_circle' or obj[0].text == 'void_square':
            #    print(self.xml)
            if obj[0].text not in select_lst:
                # remove wrong object then output
                self.root.remove(obj)
                self.tree.write(self.xml)
                '''
                # remove directly
                print('delete %s' % os.path.basename(self.xml).split('.')[0])
                os.remove(self.xml)
                os.remove(self.img)
                break
                '''

if __name__ == "__main__":


    parser = argparse.ArgumentParser()
    parser.add_argument('--xml_path', type=str, default='', help='xml file path')
    parser.add_argument('--image_path', type=str, default='', help='image file path')
    opt = parser.parse_args()
    labels = ['black_left_round', 'black_right_round', 'black_white_left', 'black_white_right',
              'blue_left_arrow', 'blue_right_arrow', 'blue_left_round', 'orange_cone', 'park_green',
              'park_white', 'park_yellow', 'blue_right_round', 'stop', 'traffic_lights_A', "traffic_lights_G",
              'traffic_lights_R', 'speed_5', 'speed_10', 'speed_25', 'speed_40', 'speed_50']
    # check XML files
    i = 0
    for xml in glob.glob(os.path.join(opt.xml_path, '*.xml')):
        if i % 1000 == 0:
            print("current deal num: ", i)
        img = os.path.join(opt.image_path, '%s.jpg' % os.path.basename(xml).split('.')[0])
        CX = CheckXml(xml, img)
        CX.check_size()
        CX.check_chn()
        #CX.check_folder()
        CX.check_filename()
        if_delete = True
        CX.check_object(labels, if_delete)
        CX.pick_object(labels)
        i += 1
