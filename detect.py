import argparse

import torch.backends.cudnn as cudnn

from models.experimental import *
from utils.datasets import *
from utils.utils import *
import time

from create_xml import GEN_Annotations


def detect(save_img=False):
    out, source, weights, view_img, save_txt, imgsz = \
        opt.output, opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size
    webcam = source == '0' or source.startswith('rtsp') or source.startswith('http') or source.endswith('.txt')

    # Initialize
    device = torch_utils.select_device(opt.device)
    if os.path.exists(out):
        shutil.rmtree(out)  # delete output folder
    os.makedirs(out)  # make new output folder
    half = device.type != 'cpu'  # half precision only supported on CUDA
    half = False
    #print(half)
    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = torch_utils.load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model'])  # load weights
        modelc.to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz)
    else:
        #save_img = True
        view_img = True
        dataset = LoadImages(source, img_size=imgsz)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
    out_xml_path = ''
    out_jpg_path = ''#机器标注的output路径
    frame_num = 0
    detect_num = 0

    for path, img, im0s, vid_cap in dataset:
        frame_num += 1
        # if frame_num %3 != 0:
        #     continue
        #print(frame_num)
        image_clone = im0s.copy()
        t1 = time.time()
        image_height,image_width,channel = im0s.shape
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)
        print("time in image precess: ", time.time() - t1)
        # Inference
        t2 = time.time()
        pred = model(img, augment=opt.augment)[0]
        #print(pred)
        print("time in detect: ", time.time() - t2)
        # Apply NMS
        t3 = time.time()
        pred = non_max_suppression1(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        print("time in nms: ", time.time() - t3)
        #save_path = os.path.join(out_jpg_path, os.path.basename(path))
        save_path = ''#视频保存路径
        #print(save_path)

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)
        image_name = os.path.basename(path)
        anno = GEN_Annotations(image_name)
        anno.set_size(image_width, image_height, channel)
        # image_name = "zc_0428_" + str(frame_num) + ".jpg"
        save_path_have = os.path.join(out_jpg_path, image_name)
        xml_name = image_name.replace('jpg', 'xml')
        xml_path = os.path.join(out_xml_path, xml_name)
        # Process detections
        for i, det in enumerate(pred):
            #print(det)# detections per image
            tt = time.time()
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            txt_path = str(Path(out) / Path(p).stem) + ('_%g' % dataset.frame if dataset.mode == 'video' else '')
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh

            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string



                # Write results
                for *xyxy, conf, cls in det:
                    #print(round(float(conf.cpu()), 2))
                    xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                    label = '%s %.2f' % (names[int(cls)], conf)
                    #label = '%.2f' % (conf)
                    #print(label)
                    xmin = int(xyxy[0])
                    ymin = int(xyxy[1])
                    xmax = int(xyxy[2])
                    ymax = int(xyxy[3])
                    label_name = names[int(cls)]
                    #针对现实世界视频屏蔽ymax坐标小于50的误报检测框
                    if ymax < 50:
                        continue
                    plot_one_box(xyxy, im0, label=label, color=colors[int(cls)], line_thickness=1)
                    anno.add_pic_attr(label_name,xmin,ymin,xmax,ymax,round(float(conf.cpu()), 2))
                #cv2.imwrite(save_path_have, im0)


            else:
                pass

            print('time in Process detections: ', time.time()- tt)
            # Stream results
            if view_img:
                #im0 = cv2.resize(im0, (1280, 720))
                cv2.imshow('111', im0)
                cv2.waitKey(1) #停1毫秒切换下一帧
                #if cv2.waitKey(1) == ord('q'):  # q to quit
                #    raise StopIteration

            # Save results (image with detections)
            #print(save_img,save_path, vid_path)
            if save_img:
                if dataset.mode == 'images':
                    print(save_path)
                    cv2.imwrite(save_path, im0)
                else:
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer

                        #fourcc = 'H264'  # output video codec
                        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
                        fps = 25
                        w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                        h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        vid_writer = cv2.VideoWriter(save_path, fourcc, fps, (w, h))
                    vid_writer.write(im0)
        #anno.savefile(xml_path)

    print("detect num: ", detect_num)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default=r'D:\donkeycar projects\projects\donkeycar\mycar\yolov5.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default=r'D:\2021S2\capstone\data2022\test2-7-20.mp4\test2-7-20.mp4', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--output', type=str, default='inference/output', help='output folder')  # output folder
    parser.add_argument('--img-size', type=int, default=416, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.7, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    opt = parser.parse_args()
    print(opt)

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt', 'yolov3-spp.pt']:
                detect()
                create_pretrained(opt.weights, opt.weights)
        else:
            detect()
