# How to run

example:

```
python train.py --img-size=416 --cfg=./models/yolov5s_se.yaml --data=./data/voc.yaml --batch-size=16 --epochs=100
```

```
python val.py --weights ./weights/yolov5s.pt --img-size 256 --task test
```

* Note that the data path should be changed in VOC. yaml and test.yaml.